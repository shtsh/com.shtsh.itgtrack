package com.shtsh.itgtrack;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

public class PacksXmlParser {
	
    // We don't use namespaces
    private static final String ns = null;
    private Context context;
   
    public List<Pack> parse(Context context, InputStream in) throws XmlPullParserException, IOException {
    	Log.d("XMLParser", "Starting parsing");
    	this.context = context;
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
            Log.d("XMLParser", "Parsing was ended");
        }
    }
    

	private List<Pack> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
		List<Pack> entries = new ArrayList<Pack>();
        parser.require(XmlPullParser.START_TAG, ns, "database");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            
            if (name.equals("pack")) {
                entries.add(readPack(parser));
            } else {
                skip(parser);
            }
        }
       
        return entries;
    }
    
    
 // Parses the contents of an entry. 
 private Pack readPack(XmlPullParser parser) throws XmlPullParserException, IOException {
	 parser.require(XmlPullParser.START_TAG, ns, "pack");
	 List<Song> allSongs = new ArrayList<Song>() ;
	 String pack = parser.getAttributeValue(null, "name");
	 while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        String name = parser.getName();
	        
	        if (name.equals("song")) {	        	
	        	allSongs = getSongs(parser, parser.getAttributeValue(null, "name"), pack );
	        } else {
	            skip(parser);
	        }
	    }
	 return new Pack(allSongs);

 }
 
 private List<Song> getSongs(XmlPullParser parser, String songName, String songPack) throws XmlPullParserException, IOException {
	 List<Song> allSongDifficulties = new ArrayList<Song>();
	 parser.require(XmlPullParser.START_TAG, ns, "song");
	 Song song = new Song();
	 while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        String name = parser.getName();
	        if (name.equals("difficulty")) {
	        	
	        	song.setPack(songPack);
	        	song.setTitle(songName);
	        	String difficulty = parser.getAttributeValue(null, "name");
	        	int position = difficulty.indexOf(" ");
	        	String firstWord;
	        	if (position > 0) {
	        		firstWord = difficulty.substring(0, position);
	        	} else {
	        		firstWord = difficulty;
	        	}
	        	
	        	String capitalizedFirstWord = firstWord.substring(0,1).toUpperCase()
	        										   .concat(firstWord.substring(1, firstWord.length()));
	        	String difficultyITGStyle;
	        	if (capitalizedFirstWord.equals("Challenge")){
	        		difficultyITGStyle = "Expert";
	        	} else if (capitalizedFirstWord.equals("Beginner")) {
	        		difficultyITGStyle = "Novice";
	        	} else {
	        		difficultyITGStyle = capitalizedFirstWord;
	        	}
	        	song.setDifficulty(difficultyITGStyle);
	        	song.setFeet(Integer.valueOf(parser.getAttributeValue(null, "feet")));
	        	MySQLiteHelper db = new MySQLiteHelper(context);
	        	db.addSong(song);
	        	if(parser.getAttributeValue(null,"result") != null) {
	        		song.setResult(Float.valueOf(parser.getAttributeValue(null,"result")));
	        		db.addResult(song);
	        	}
	        	db.close();
	        	
	        	allSongDifficulties.add(song);
	        	toEndOfDifficulty(parser);
	        } else {
	        	skip(parser);
	        }
	 }
	 return allSongDifficulties;

 }
 
 private void toEndOfDifficulty (XmlPullParser parser) throws XmlPullParserException, IOException {
	 parser.require(XmlPullParser.START_TAG, ns, "difficulty");

	 while (parser.next() != XmlPullParser.END_TAG) {
	 }

 }
 
private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
	    if (parser.getEventType() != XmlPullParser.START_TAG) {
	        throw new IllegalStateException();
	    }
	    int depth = 1;
	    while (depth != 0) {
	        switch (parser.next()) {
	        case XmlPullParser.END_TAG:
	            depth--;
	            break;
	        case XmlPullParser.START_TAG:
	            depth++;
	            break;
	        }
	    }
	 }

}
