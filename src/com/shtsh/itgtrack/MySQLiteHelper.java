package com.shtsh.itgtrack;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper  extends SQLiteOpenHelper  {

	private static final int DATABASE_VERSION = 12;

    private static final String DATABASE_NAME = "stats";
    
    private static final String RESULTS_TABLE_NAME = "results";
    private static final String RESULTS_ID = "ID";
    private static final String RESULTS_SONG = "SONG_ID";  
    private static final String RESULTS_RESULT = "RESULT";
    
    private static final String SONGS_TABLE_NAME = "songs";
    private static final String SONGS_ID = "ID";
    private static final String SONGS_PACK_ID = "PACK_ID";
    private static final String SONGS_SONG = "SONG";
    private static final String SONGS_DIFFICULTY = "DIFFICULTY";
    private static final String SONGS_FEET = "FEET";
    
    private static final String PACKS_TABLE_NAME = "packs";
    private static final String PACKS_ID = "ID";
    private static final String PACKS_PACK = "NAME";
    
    private static SQLiteDatabase db;

                		
    private static final String RESULTS_TABLE_CREATE =
            "CREATE TABLE " + RESULTS_TABLE_NAME + " (" +
            		RESULTS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            		RESULTS_SONG + " INTEGER not null," +
            		RESULTS_RESULT + " real not null" + 
            		" );";
    
    private static final String SONGS_TABLE_CREATE =
            "CREATE TABLE " + SONGS_TABLE_NAME + " (" +
            		SONGS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            		SONGS_PACK_ID + " INTEGER not null, " +
            		SONGS_SONG + " TEXT not null, " +
            		SONGS_DIFFICULTY + " TEXT not null, " +
            		SONGS_FEET + " INTEGER " +
            		" );";   
    
    private static final String PACKS_TABLE_CREATE =
            "CREATE TABLE " + PACKS_TABLE_NAME + " (" +
            		PACKS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            		PACKS_PACK + " TEXT not null " +
            		" );";  
    
    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = this.getWritableDatabase();
    }
    
    public void close(){
		if (db!=null){
		    db.close();
		}
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    	Log.w(MySQLiteHelper.class.getName(),"Creating table "+ RESULTS_TABLE_NAME);
    	Log.w(MySQLiteHelper.class.getName(),"Creating table "+ SONGS_TABLE_NAME);
    	Log.w(MySQLiteHelper.class.getName(),"Creating table "+ PACKS_TABLE_NAME);
    	Log.d("SQL Query: ", RESULTS_TABLE_CREATE);
    	Log.d("SQL Query: ", SONGS_TABLE_CREATE);
    	Log.d("SQL Query: ", PACKS_TABLE_CREATE);
        db.execSQL(RESULTS_TABLE_CREATE);
        db.execSQL(SONGS_TABLE_CREATE);
        db.execSQL(PACKS_TABLE_CREATE);
	    db.execSQL("CREATE INDEX " + SONGS_PACK_ID + "_idx ON " + SONGS_TABLE_NAME + "(" + SONGS_PACK_ID + ")");
	    db.execSQL("CREATE INDEX " + SONGS_SONG + "_idx ON " + SONGS_TABLE_NAME + "(" + SONGS_SONG + ")");
	    db.execSQL("CREATE INDEX " + SONGS_DIFFICULTY + "_idx ON " + SONGS_TABLE_NAME + "(" + SONGS_DIFFICULTY + ")");
	    db.execSQL("CREATE INDEX " + RESULTS_RESULT + "_idx ON " + RESULTS_TABLE_NAME + "(" + RESULTS_RESULT + ")");
	    db.execSQL("CREATE INDEX " + PACKS_PACK + "_idx ON " + PACKS_TABLE_NAME + "(" + PACKS_PACK + ")");

    }
    
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    Log.w(MySQLiteHelper.class.getName(),
	            "Upgrading database from version " + oldVersion + " to " + newVersion);
	    	if (oldVersion < 10) {
	    		db.execSQL("DELETE FROM " + SONGS_TABLE_NAME + " WHERE " + SONGS_SONG + " LIKE '%''''%'");
	    	} else if (oldVersion < 12) {
	    		db.execSQL(	"UPDATE " + PACKS_TABLE_NAME + 
	    					" SET " + PACKS_PACK + "=replace(" +  PACKS_PACK + ", '''''', '''')");
	    	}
	    		Log.d("Upgrade DB", "Finished");
		
	}
	
	public int getPackIdByName(String packName){
		int id = 0;
		String selectQuery = "SELECT " +
				PACKS_ID + 
				" FROM " + PACKS_TABLE_NAME +
				" WHERE " + PACKS_PACK + "='" + packName.replace("'", "''") + "' " +
				";";
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	            id = cursor.getInt(0);
	        } while (cursor.moveToNext());
	    }
		cursor.close();
		return id;
	}
	
	public String getPackNameById(int id){
		
		String packName = "";
		String selectQuery = "SELECT " +
				PACKS_PACK + 
				" FROM " + PACKS_TABLE_NAME +
				" WHERE " + PACKS_ID + "=" + Integer.toString(id) + " " +
				";";
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	            packName = cursor.getString(0);
	        } while (cursor.moveToNext());
	    } 
		cursor.close();
		return packName;
	}
	
	public int getSongId(Song song){
		int id = 0;
		String selectQuery = "SELECT " +
				SONGS_ID + 
				" FROM " + SONGS_TABLE_NAME +
				" WHERE " + SONGS_PACK_ID + "='" + Integer.toString(getPackIdByName(song.getPack())) + "' " +
				" AND " + SONGS_SONG + "='" + song.getTitle().replace("'", "''") + "' " +
				" AND " + SONGS_DIFFICULTY + "='" + song.getDifficulty() + "' " +
				";";
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	            id = cursor.getInt(0);
	        } while (cursor.moveToNext());
	    }
		return id;
	}
	
	public void addResult (Song song){
				
		if (! this.songExists(song)) {
			this.addSong(song);
		}
		
		if (this.getResult(song) == 0) { 
			ContentValues values = new ContentValues();	
			values.put(RESULTS_SONG, this.getSongId(song)); 
			values.put(RESULTS_RESULT, song.getResult());
			db.insert(RESULTS_TABLE_NAME, null, values);
		} else {
			this.updateResult(song);
		}
		
	}
	
	// Add pack
	public void addPack (String packName){
		if (!this.packExists(packName)) {
			ContentValues values = new ContentValues();	
			values.put(PACKS_PACK, packName); 
			db.insert(PACKS_TABLE_NAME, null, values);
		}
	}
	
	// Add song without results
	public void addSong (Song song){
		if (!this.songExists(song)) {
			this.addPack(song.getPack());
			ContentValues values = new ContentValues();	
			values.put(SONGS_PACK_ID, this.getPackIdByName(song.getPack())); 
			values.put(SONGS_SONG, song.getTitle());
			values.put(SONGS_DIFFICULTY, song.getDifficulty());
			values.put(SONGS_FEET, song.getFeet());
			db.insert(SONGS_TABLE_NAME, null, values);
		}
	}
	
	public Song getSongById(int id){
		Song song = new Song();
		String selectQuery = "SELECT " +
				SONGS_PACK_ID + ", " +
				SONGS_SONG + ", " +
				SONGS_DIFFICULTY + ", " +
				SONGS_FEET +
				" FROM " + SONGS_TABLE_NAME +
				" WHERE " + SONGS_ID + "=" + Integer.toString(id) +
				";";
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	            song.setPack(this.getPackNameById(cursor.getInt(0)));
	            song.setTitle(cursor.getString(1));
	            song.setDifficulty(cursor.getString(2)
	            		);
	            song.setFeet(cursor.getInt(3));
	        } while (cursor.moveToNext());
	    }
		return song;
	}
	
	public void updateResult (Song song){
		// Update if new value bigger than old
		if (this.getResult(song) == 0) {
			this.addResult(song);
		}
		if (this.getResult(song) < song.getResult()) {
			ContentValues values = new ContentValues();
			int id = getSongId(song);
			values.put(RESULTS_SONG, id); 
			values.put(RESULTS_RESULT, song.getResult());
			db.update(	RESULTS_TABLE_NAME, 
						values, 
						RESULTS_SONG + "=?",
						new String[] { Integer.toString(id) }
					);
			}

	}
	
	public void deleteResult (Song song){
		ContentValues values = new ContentValues();
		int id = getSongId(song);
		values.put(RESULTS_SONG, id); 
		values.put(RESULTS_RESULT, song.getResult());
		db.delete(	RESULTS_TABLE_NAME, 
					RESULTS_SONG + "=?",
					new String[] { Integer.toString(id) }
				);
	}
	
	public void deleteSong (Song song){
		int id = getSongId(song);
		db.delete(	RESULTS_TABLE_NAME, 
					RESULTS_SONG + "=?",
					new String[] { Integer.toString(id) }
				);
		db.delete(	SONGS_TABLE_NAME, 
					SONGS_ID + "=?",
					new String[] { Integer.toString(id) }
				);
	}
	
	public float getResult (Song song){
		long startTime = System.currentTimeMillis();
		float result = 0;
		String selectQuery = "SELECT " +
				RESULTS_RESULT + 
				" FROM " + RESULTS_TABLE_NAME +
				" WHERE " + RESULTS_SONG + "=" + Integer.toString(getSongId(song));
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	            result = cursor.getFloat(0);
	        } while (cursor.moveToNext());
	    }	
		Log.d("getResult(" + song.getTitle() + ")",Long.toString(System.currentTimeMillis() - startTime) + " ms");
		return result;
	}
	
	public List<Song> getAllSongsWithResults(){
		List<Song> songsList = new ArrayList<Song>();
		String selectQuery = "SELECT " +
				RESULTS_SONG + ", " +
				RESULTS_RESULT +	
				" FROM " + RESULTS_TABLE_NAME;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	            Song song = new Song();
	            int id = cursor.getInt(0);
	            song = this.getSongById(id);
	            song.setResult(Float.parseFloat(cursor.getString(1)));
	            songsList.add(song);
	        } while (cursor.moveToNext());
	    }

		return songsList;
	}
	
	public int getSongsCountWithResults(){
		String countQuery = "SELECT * from " + RESULTS_TABLE_NAME;
		Cursor cursor = db.rawQuery(countQuery, null);
		int value = cursor.getCount();
		return value;
	}
	
	public int getSongsCount(){
		String countQuery = "SELECT * from " + SONGS_TABLE_NAME;
		Cursor cursor = db.rawQuery(countQuery, null);
		int value = cursor.getCount();
		return value;
	}
	
	public float getAvgScore(){
		float SumScores = 0;
		for (Song song : this.getAllSongsWithResults()){
			SumScores += song.getResult();
		} 
		float SongsCount = (float)this.getSongsCountWithResults();
		return SumScores/SongsCount;
	}
	
	// Get Packs list sorted by name
	public List<String> getPackNamesWithResults() {
		List<String> packs = new ArrayList<String>();
		String query = "SELECT DISTINCT " + SONGS_PACK_ID + 
					   " FROM " + SONGS_TABLE_NAME +
					   " WHERE " + SONGS_ID + 
					   " IN (SELECT " + RESULTS_SONG +" FROM " + RESULTS_TABLE_NAME + ")" +
					   " ORDER BY " + SONGS_PACK_ID + " ASC";
		Cursor cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()) {
	        do {
	            packs.add(this.getPackNameById(cursor.getInt(0)));
	        } while (cursor.moveToNext());
	    }

		return packs;
	}
	
	public List<String> getPacks() {
		// Return all available packs
		List<String> packs = new ArrayList<String>();
		String query = "SELECT DISTINCT " + PACKS_PACK + 
					   " FROM " + PACKS_TABLE_NAME +
					   " ORDER BY " + PACKS_PACK + " ASC";
		Cursor cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()) {
	        do {
	            packs.add(cursor.getString(0));
	        } while (cursor.moveToNext());
	    }
		return packs;
	}
	
	public Pack getPack(String packName, boolean withResultsOnly) {
		// Return  packs with name
		Pack pack = new Pack(packName);
		List<Song> songs;
		if (withResultsOnly) {
			songs = this.getSongsForPackWithResults(packName);
		} else {
			songs = this.getSongsForPack(packName);
		}
		for (Song song : songs){
			pack.addSong(song);
		}
		return pack;
	}
	
	// Get all songs with results for pack
	public List<Song> getSongsForPackWithResults(String pack){
		List<Song> songsList = new ArrayList<Song>();
		String selectQuery = "SELECT " +
				SONGS_PACK_ID + ", " +
				SONGS_SONG + ", " +
				SONGS_DIFFICULTY + ", " +
				SONGS_FEET +
				" FROM " + SONGS_TABLE_NAME +
				" WHERE " + SONGS_PACK_ID + "=" + Integer.toString(this.getPackIdByName(pack)) +" AND " +
				SONGS_ID + " IN ( SELECT " + RESULTS_SONG + " FROM " + RESULTS_TABLE_NAME + ")";
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	            Song song = new Song();
	            song.setPack(this.getPackNameById(cursor.getInt(0)));
	            song.setTitle(cursor.getString(1));
	            song.setDifficulty(cursor.getString(2));
	            song.setFeet(Integer.parseInt(cursor.getString(3)));
	            song.setResult(this.getResult(song));
	            songsList.add(song);
	        } while (cursor.moveToNext());
	    }
		cursor.close();
		return songsList;
	}
	
	// Get all songs for pack
	public List<Song> getSongsForPack(String pack){
		long startTime = System.currentTimeMillis();
		List<Song> songsList = new ArrayList<Song>();
		String selectQuery = "SELECT " +
				SONGS_PACK_ID + ", " +
				SONGS_SONG + ", " +
				SONGS_DIFFICULTY + ", " +
				SONGS_FEET  +
				" FROM " + SONGS_TABLE_NAME +
				" WHERE " + SONGS_PACK_ID + "=" + Integer.toString(this.getPackIdByName(pack)) + ";";
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	            Song song = new Song();
	            song.setPack(this.getPackNameById(cursor.getInt(0)));
	            song.setTitle(cursor.getString(1));
	            song.setDifficulty(cursor.getString(2));
	            song.setFeet(Integer.parseInt(cursor.getString(3)));
	            songsList.add(song);
	        } while (cursor.moveToNext());
	    }
		cursor.close();
		Log.d("getSongsForPack(" +  pack + ")", Long.toString(System.currentTimeMillis() - startTime) + " ms");
		return songsList;
	}
	
	// Get all songs for pack
	public List<Song> getSongsWithResultsForPack(String pack){
		long startTime = System.currentTimeMillis();
		List<Song> songsList = new ArrayList<Song>();
		String selectQuery = "SELECT " +
				SONGS_PACK_ID + ", " +
				SONGS_SONG + ", " +
				SONGS_DIFFICULTY + ", " +
				SONGS_FEET  +
				" FROM " + SONGS_TABLE_NAME +
				" WHERE " + SONGS_PACK_ID + "=" + Integer.toString(this.getPackIdByName(pack)) + 
				" AND " + SONGS_ID + 
					" IN ( SELECT DISTINCT " + RESULTS_SONG + 
					" FROM " + RESULTS_TABLE_NAME + ")" +
				";";
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
	        do {
	            Song song = new Song();
	            song.setPack(this.getPackNameById(cursor.getInt(0)));
	            song.setTitle(cursor.getString(1));
	            song.setDifficulty(cursor.getString(2));
	            song.setFeet(Integer.parseInt(cursor.getString(3)));
	            song.setResult(this.getResult(song));
	            songsList.add(song);
	        } while (cursor.moveToNext());
	    }
		cursor.close();
		Log.d("getSongsForPack(" +  pack + ")", Long.toString(System.currentTimeMillis() - startTime) + " ms");
		return songsList;
	}
	
	public boolean songExists(Song song){
		boolean existance = false;
		
		String selectQuery = "SELECT " +
				SONGS_ID + 
				" FROM " + SONGS_TABLE_NAME +
				" WHERE " + SONGS_PACK_ID + "='" + Integer.toString(this.getPackIdByName(song.getPack())) + "'" +
				" AND " + SONGS_SONG + "='" + song.getTitle().replace("'", "''") + "'" +
				" AND " + SONGS_DIFFICULTY + "='" + song.getDifficulty() + "'"
				;
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			existance = true;
	    }
		cursor.close();
		return existance;
	}
	
	public boolean packExists(String packName){
		boolean existance = false;
		
		String selectQuery = "SELECT " +
				PACKS_ID + 
				" FROM " + PACKS_TABLE_NAME +
				" WHERE " + PACKS_PACK + "='" + packName.replace("'", "''") + "'" +
				";";
		Cursor cursor = db.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			existance = true;
	    }
		cursor.close();
		return existance;
	}
	
	public List<String> getDifficulties(){
		// Return all available packs
		List<String> difficulties = new ArrayList<String>();
		String query = "SELECT DISTINCT " + SONGS_DIFFICULTY + 
					   " FROM " + SONGS_TABLE_NAME +
					   " ORDER BY " + SONGS_DIFFICULTY + " ASC";
		Cursor cursor = db.rawQuery(query, null);
		if (cursor.moveToFirst()) {
	        do {
	            difficulties.add(cursor.getString(0));
	        } while (cursor.moveToNext());
	    }
		return difficulties;
	}
}
