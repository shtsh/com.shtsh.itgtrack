package com.shtsh.itgtrack;

import java.io.File;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class EditResult extends Activity {

	private Spinner difficulties;
	private MySQLiteHelper db;
	private Song song;
	private Intent intent_home;
	private SharedPreferences prefs;
	private String mCurrentPhotoPath;
	private static final int CAMERA_REQUEST = 1888; 
	private ImageView mImageView;
	private boolean isClickable;

	public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add);
        isClickable = false;
        prefs = this.getSharedPreferences("com.sthsh.itgtrack", Context.MODE_PRIVATE);
       
        intent_home = new Intent(this, MainActivity.class);
        intent_home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        db = new MySQLiteHelper(this);
        Intent i = getIntent();
        song = (Song) i.getParcelableExtra("song");
        
		// Possibility to return to the main activity
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		// Restoring data to Activity
		EditText edit;
		EditText editResult = (EditText)findViewById(R.id.add_result);
		
		// Save result when press done
		editResult.setOnEditorActionListener(new OnEditorActionListener() {        
			
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
		        if(actionId==EditorInfo.IME_ACTION_DONE){
		            EditResult.this.clickSave();
		        }
				return false;
			}
		});
		
		
		if (song != null){
			edit = (EditText)findViewById(R.id.add_song_name);
			edit.setText(song.getTitle());
			edit.setEnabled(false);
			edit.setTextColor(Color.WHITE);
			edit = (EditText)findViewById(R.id.add_pack_name);
			edit.setText(song.getPack());
			edit.setEnabled(false);
			edit.setTextColor(Color.WHITE);
			editResult.setText(Float.toString(song.getResult()));
			editResult.setSelectAllOnFocus(true);
			editResult.requestFocus();
			getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
			edit = (EditText)findViewById(R.id.add_feet);
			edit.setText(Integer.toString(song.getFeet()));
			edit.setEnabled(false);
			edit.setTextColor(Color.WHITE);
    	
			difficulties = (Spinner) findViewById(R.id.itg_difficulties);
			List<String> allDifficulities;
			allDifficulities = db.getDifficulties();
			ArrayAdapter<String> adapter = null;
			if (allDifficulities != null) {
				adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,allDifficulities );
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				int spinnerPosition = adapter.getPosition(song.getDifficulty());
				difficulties.setAdapter(adapter);
				difficulties.setSelection(spinnerPosition);
				difficulties.setEnabled(false);
			}			
		}

	}
	
	@Override
	public void onWindowFocusChanged (boolean hasFocus) {

		mCurrentPhotoPath = Environment.getExternalStorageDirectory()
				+ prefs.getString("application_sdcard_directory",
						"/Android/data/com.shtsh.itgtrack")
				+ song.generateFileName() 
				+ ".png";
		File photoFile = new File(mCurrentPhotoPath);
		if (photoFile.exists()) {
			Log.d("mCurrentPhotoPath", mCurrentPhotoPath);
			
			mImageView = (ImageView) findViewById(R.id.add_image);
			// Get the dimensions of the View
			int targetW = mImageView.getWidth();
			int targetH = mImageView.getHeight();

			// Get the dimensions of the bitmap
			BitmapFactory.Options bmOptions = new BitmapFactory.Options();
			bmOptions.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
			int photoW = bmOptions.outWidth;
			int photoH = bmOptions.outHeight;

			// Determine how much to scale down the image
			int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

			// Decode the image file into a Bitmap sized to fill the View
			bmOptions.inJustDecodeBounds = false;
			bmOptions.inSampleSize = scaleFactor;
			bmOptions.inPurgeable = true;

			Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath,
					bmOptions);
			mImageView.setImageBitmap(bitmap);
			isClickable = true;

			mImageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (isClickable) {
						Intent intent = new Intent();
						intent.setAction(Intent.ACTION_VIEW);
						intent.setDataAndType(
								Uri.parse("file://" + mCurrentPhotoPath),
								"image/*");
						startActivity(intent);
					}

				}
			});

		}
	}
	
    //Action Bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit, menu);
		// Don't show "Delete Result" button if we don't have a result
		if (song.getResult() == 0) {
			MenuItem item = menu.findItem(R.id.edit_delete);
			item.setVisible(false);
		}
        return true;
    }
    
	// Menu
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
	        
		    switch (item.getItemId()) {
		        case android.R.id.home:
		            // app icon in action bar clicked; go home
		            startActivity(intent_home);
		            return true;
		            
		        case R.id.edit_share_result:
		        	share();
		        	return true;
		        case R.id.edit_save:
		        	clickSave();
		        	return true;
		        case R.id.edit_photo:
					Intent takePictureIntent = new Intent(
							MediaStore.ACTION_IMAGE_CAPTURE);
					String appFolderName = Environment.getExternalStorageDirectory()
							+ prefs.getString("application_sdcard_directory",
									"/Android/data/com.shtsh.itgtrack");
					if (Environment.MEDIA_MOUNTED.equals(Environment
							.getExternalStorageState())
							&& !Environment.MEDIA_MOUNTED_READ_ONLY.equals(Environment
									.getExternalStorageState())) {
						String photoName = song.generateFileName();

						mCurrentPhotoPath = appFolderName + photoName + ".png";
						File photoFile = new File(mCurrentPhotoPath);
						if (photoFile.exists()) photoFile.delete();
						Uri uriSavedImage = Uri.fromFile(photoFile);
						Log.d("Photo Saving Folder",appFolderName + photoName + ".png");
						takePictureIntent.putExtra("output", uriSavedImage);
						startActivityForResult(takePictureIntent, CAMERA_REQUEST);
					}
		        	return true;
		        case R.id.edit_delete_photo:
		        	final File photoFile = new File(mCurrentPhotoPath);
		        	if (photoFile.exists()){
			        	new AlertDialog.Builder(this)
		        		.setTitle("Result Deletion")
		        		.setMessage("Do you want to delete photo?")
		        		.setIcon(android.R.drawable.ic_dialog_alert)
		        		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		        			public void onClick(DialogInterface dialog, int whichButton) {
		    		        	photoFile.delete();	
		    		        	mImageView.setImageBitmap(null);	
		    		        	isClickable = false;
		    		        }})
		    		        
		        		.setNegativeButton(android.R.string.no, null).show();
		        	}
		        	return true;
		        case R.id.edit_delete:
		        	
		        	new AlertDialog.Builder(this)
		        		.setTitle("Result Deletion")
		        		.setMessage("Do you want to delete result for\n" +
		        				song.getPack() + " / " + song.getTitle() +
		        				" " + song.getDifficulty() + " ("+
		        				song.getFeet() + ")" +
		        				"?")
		        		.setIcon(android.R.drawable.ic_dialog_alert)
		        		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

		        			public void onClick(DialogInterface dialog, int whichButton) {
		    		        	db.deleteResult(song);
		    		        	startActivity(intent_home);		        			
		    		        }})
		    		        
		        		.setNegativeButton(android.R.string.no, null).show();

		        	return true;
		        case R.id.edit_delete_song:
		        	new AlertDialog.Builder(this)
	        		.setTitle("Result Deletion")
	        		.setMessage("Do you want to delete Song\n" +
	        				song.getPack() + " / " + song.getTitle() +
	        				" " + song.getDifficulty() + " ("+
	        				song.getFeet() + ")" +
	        				"?")
	        		.setIcon(android.R.drawable.ic_dialog_alert)
	        		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

	        			public void onClick(DialogInterface dialog, int whichButton) {
	    		        	db.deleteSong(song);
	    		        	startActivity(intent_home);		        			
	    		        }})
	    		        
	        		.setNegativeButton(android.R.string.no, null).show();

		        	return true;
		        default:
		            return super.onOptionsItemSelected(item);
		    }
		}
		
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
	        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) { 
	        	ImageView mImageView = (ImageView) findViewById(R.id.add_image);
	        	 // Get the dimensions of the View
	            int targetW = mImageView.getWidth();
	            int targetH = mImageView.getHeight();
	            // Get the dimensions of the bitmap
	            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
	            bmOptions.inJustDecodeBounds = true;
	            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
	            int photoW = bmOptions.outWidth;
	            int photoH = bmOptions.outHeight;
	          
	            // Determine how much to scale down the image
	            int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
	          
	            // Decode the image file into a Bitmap sized to fill the View
	            bmOptions.inJustDecodeBounds = false;
	            bmOptions.inSampleSize = scaleFactor;
	            bmOptions.inPurgeable = true;
	          
	            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
	            mImageView.setImageBitmap(bitmap);
	            isClickable = true;
	            
	        }  
	    } 
		
		private void clickSave(){
        	EditText edit = (EditText)findViewById(R.id.add_result);
        	if (!edit.getText().toString().equals("")) {
        		song.setResult(Float.parseFloat(edit.getText().toString()));
        		db.updateResult(song);
        		startActivity(intent_home);
        	} else {
        		Toast.makeText(this, "No result specified", Toast.LENGTH_SHORT).show();
        	}
    	}
		
		private void share(){
			String message = "I passed " + song.getPack() + " / " 
						+ song.getTitle() 
						+ " (" + song.getDifficulty() + " " + Integer.toString(song.getFeet())+ ")"
						+ " at " + song.getResult() + "%";
			Intent share = new Intent(Intent.ACTION_SEND);
			share.setType("text/plain");
			share.putExtra(Intent.EXTRA_TEXT, message);
			startActivity(Intent.createChooser(share, "Share result"));
		}
		
		@Override
		protected void onDestroy(){
			super.onDestroy();
			db.close();
		}
}
