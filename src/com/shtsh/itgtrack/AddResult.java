package com.shtsh.itgtrack;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class AddResult extends Activity {

	private Spinner difficulties;
	private MySQLiteHelper db;
	private String pack;
	private Pack selectedPack = new Pack();
	private Song selectedSong;
	private Intent intent_home;
	private static final int CAMERA_REQUEST = 1888; 
	SharedPreferences prefs;
	private String mCurrentPhotoPath;
	String emptyName;
	File earlierPicture;

	public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        emptyName = "/empty";
        intent_home = new Intent(this, MainActivity.class);
        prefs = this.getSharedPreferences("com.sthsh.itgtrack", Context.MODE_PRIVATE);
        db = new MySQLiteHelper(this);
        
        Intent i = getIntent();
        pack = i.getStringExtra("pack");
        
		setContentView(R.layout.add);
		// Possibility to return to the main activity
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		

		// Spinner with difficulties
		difficulties = (Spinner) findViewById(R.id.itg_difficulties);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.itg_difficulties, android.R.layout.simple_spinner_dropdown_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		difficulties.setAdapter(adapter);
		
		//Autocompletion for pack and song name
		List<String> packsList = db.getPacks();
		
		String[] packs = packsList.toArray(new String[packsList.size()]);
		
		ArrayAdapter<String> packsAdapter = new ArrayAdapter<String> (this,android.R.layout.simple_list_item_1,packs);
		final AutoCompleteTextView editPack = (AutoCompleteTextView)findViewById(R.id.add_pack_name);
		final AutoCompleteTextView editSong = (AutoCompleteTextView)findViewById(R.id.add_song_name);
		final EditText songFeet = (EditText) findViewById(R.id.add_feet);
		getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		editPack.setAdapter(packsAdapter);
		editPack.setThreshold(1);
		
		if (pack != null) {
			editPack.setText(pack);
			List<String> songNames = new ArrayList<String>();
			String currentPack = editPack.getText().toString();
			
			selectedPack = db.getPack(currentPack,false);

			for (Song song : selectedPack.getAllSongs()){
				songNames.add(song.getTitle());
			}
			String[] songs = songNames.toArray(new String[songNames.size()]);
			// make songs array unique
			Set<String> temp = new HashSet<String>(Arrays.asList(songs));
			songs = temp.toArray(new String[temp.size()]);			
			
			ArrayAdapter<String> songsAdapter = new ArrayAdapter<String> (AddResult.this,android.R.layout.simple_list_item_1,songs);
			
			editSong.setThreshold(1);
			editSong.setAdapter(songsAdapter);	
			
		}
		
		// Create Adapter to song name when pack name is selected from the list
		editPack.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				List<String> songNames = new ArrayList<String>();
				String currentPack = editPack.getText().toString();
				
				selectedPack = db.getPack(currentPack,false);
				
				for (Song song : selectedPack.getAllSongs()){
					songNames.add(song.getTitle());
				}
				String[] songs = songNames.toArray(new String[songNames.size()]);
				// make songs array unique
				Set<String> temp = new HashSet<String>(Arrays.asList(songs));
				songs = temp.toArray(new String[temp.size()]);		
				ArrayAdapter<String> songsAdapter = new ArrayAdapter<String> (AddResult.this,android.R.layout.simple_list_item_1,songs);
				editSong.setThreshold(1);
				editSong.setAdapter(songsAdapter);				
			}
			
		});
		
		editSong.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				List<String> difficultiesForSong = new ArrayList<String>();				
				String currentSong = editSong.getText().toString();
				for (Song song : selectedPack.getAllDifficulties(currentSong)){
					difficultiesForSong.add(song.getDifficulty());
				}
				ArrayAdapter<String> adapter = new ArrayAdapter<String> (AddResult.this,android.R.layout.simple_spinner_dropdown_item,difficultiesForSong);
 				difficulties.setAdapter(adapter);
			}
		});

		difficulties.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				selectedSong = new Song();
				selectedSong.setTitle(editSong.getText().toString());
				selectedSong.setDifficulty(difficulties.getSelectedItem().toString());
				selectedSong.setPack(selectedPack.getName());
				

				for (Song song: selectedPack.getAllDifficulties(selectedSong.getTitle())){
					if (song.getDifficulty().equals(selectedSong.getDifficulty())){
						selectedSong.setFeet(song.getFeet());
						selectedSong.setTitle(song.getTitle());
						songFeet.setText(Integer.toString(selectedSong.getFeet()));
					}
				}
				
			} 

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {				
			}

		}); 
			
		
		EditText addResult = (EditText)findViewById(R.id.add_result);
		
		// Save result when press done
		addResult.setOnEditorActionListener(new OnEditorActionListener() {        
			
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
		        if(actionId==EditorInfo.IME_ACTION_DONE){
		        	AddResult.this.saveResult();
		        }
				return false;
			}
		});
		
		// delete file with screenshot if exist
		earlierPicture = new File(Environment.getExternalStorageDirectory()
				+ prefs.getString("application_sdcard_directory",
						"/Android/data/com.shtsh.itgtrack") + emptyName + ".png");
		if (earlierPicture.exists()) earlierPicture.delete();
		
	}
	
    //Action Bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add, menu);
        return true;
    }
    
	// Menu
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			startActivity(intent_home);
			return true;
		case R.id.add_save:
			saveResult();
			return true;
		case R.id.add_photo:
			Intent takePictureIntent = new Intent(
					MediaStore.ACTION_IMAGE_CAPTURE);
			String appFolderName = Environment.getExternalStorageDirectory()
					+ prefs.getString("application_sdcard_directory",
							"/Android/data/com.shtsh.itgtrack");
			if (Environment.MEDIA_MOUNTED.equals(Environment
					.getExternalStorageState())
					&& !Environment.MEDIA_MOUNTED_READ_ONLY.equals(Environment
							.getExternalStorageState())) {
				String photoName;
				if (selectedSong.getTitle().equals("") || selectedSong.getPack().equals("")) {
					photoName = emptyName;
				} else {
					photoName = selectedSong.generateFileName();
				}
				mCurrentPhotoPath = appFolderName + photoName + ".png";
				File photoFile = new File(mCurrentPhotoPath);
				if (photoFile.exists()) photoFile.delete();
				Uri uriSavedImage = Uri.fromFile(photoFile);
				Log.d("Photo Saving Folder",appFolderName + photoName + ".png");
				takePictureIntent.putExtra("output", uriSavedImage);
				startActivityForResult(takePictureIntent, CAMERA_REQUEST);
			}
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) { 
        	ImageView mImageView = (ImageView) findViewById(R.id.add_image);
        	 // Get the dimensions of the View
            int targetW = mImageView.getWidth();
            int targetH = mImageView.getHeight();
            Log.d("tag", Integer.toString(targetH));
			Log.d("tag", Integer.toString(targetW));
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
          
            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
          
            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;
          
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            mImageView.setImageBitmap(bitmap);
            
        }  
    } 
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		db.close();
	}
	
	private void saveResult(){
		Intent intent_home = new Intent(this, MainActivity.class);
        intent_home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    	Song song = new Song();
    	EditText editSong = (AutoCompleteTextView)findViewById(R.id.add_song_name);
    	if (!editSong.getText().toString().equals(""))
    		song.setTitle(editSong.getText().toString());
    	EditText editPack = (AutoCompleteTextView)findViewById(R.id.add_pack_name);
    	if (!editPack.getText().toString().equals(""))
    		song.setPack(editPack.getText().toString());
    	EditText editResult = (EditText)findViewById(R.id.add_result);
    	if (!editResult.getText().toString().equals("") )
    		song.setResult(Float.parseFloat(editResult.getText().toString()));
    	EditText editFeet = (EditText)findViewById(R.id.add_feet);
    	if (!editFeet.getText().toString().equals(""))
    		song.setFeet(Integer.parseInt(editFeet.getText().toString()));
    	song.setDifficulty(difficulties.getSelectedItem().toString());
    	
    	if (!song.getPack().equals("") && !song.getTitle().equals("") && !song.getDifficulty().equals("") && song.getResult() > 0){
    		db.addResult(song);
    		//rename picture to correct name if song was selected
    		if (earlierPicture.exists()){
    			File newFile = new File(Environment.getExternalStorageDirectory()
    					+ prefs.getString("application_sdcard_directory",
    							"/Android/data/com.shtsh.itgtrack") + selectedSong.generateFileName() + ".png");
    			Log.d("Filename",selectedSong.generateFileName());
    			earlierPicture.renameTo(newFile);
    		}
    		startActivity(intent_home);
    	} else {
    		if (song.getPack() == "") {
    			Toast.makeText(this, "No pack specified", Toast.LENGTH_SHORT).show();
    		} else if (song.getResult() == 0 && !song.getPack().equals("") && ! song.getTitle().equals("") && !song.getDifficulty().equals("")) {
    			db.addSong(song);
    			startActivity(intent_home);
    		} else if (! song.getTitle().equals("")) {
    			Toast.makeText(this, "No song specified", Toast.LENGTH_SHORT).show();
    		} else {
    			Toast.makeText(this, "No difficulty selected", Toast.LENGTH_SHORT).show();
    		}
    	}	        	
	}
}
