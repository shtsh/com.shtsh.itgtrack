package com.shtsh.itgtrack;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class Pack {
	private List<Song> songs;
	private String name;
	
    public Pack(){
    	songs = new ArrayList<Song>();
    	name = "";
    }	
    
    public Pack(String name){
    	songs = new ArrayList<Song>();
    	this.name = name;
    }
    
    public Pack(List<Song> songs){
    	this.songs = songs;
    	this.name = "";
    }
    
    public Pack(String name, List<Song> songs){
    	this.name = name;
    	this.songs = songs;
    }
    
    public String getName(){
    	return name;
    }
    
    public void setName(String name){
    	this.name = name;
    }
    
    public void addSong(Song song){
    	if (!this.songExists(song))
    		songs.add(song);
    }
    
    public void delSong(int songPosition){
    	songs.remove(songPosition);
    }
    
    public List<Song> getAllSongs(){
    	return songs;
    }

    public void log() {
    	Log.d("Pack data", "===============================");
    	Log.d("Pack name", this.getName());
    	for (Song song : this.getAllSongs()){
    		song.log();
    	}
    }
    
    public List<Song> getAllDifficulties(String songTitle){
    	List<Song> similarSongs = new ArrayList<Song>();
    	for (Song currentSong : this.getAllSongs()){
    		if (currentSong.getTitle().equals(songTitle))
    			similarSongs.add(currentSong);
    	}
		return similarSongs;   	
    }
    
    public List<Song> getSongsWithResults(){
    	List<Song> allSongs = this.getAllSongs();
    	List<Song> newSongs = new ArrayList<Song>();
    	
    	for (Song song : allSongs) {
    		if (song.getResult() > 0) {
    			newSongs.add(song);
    		}
    	}
    	return newSongs;
    }
    
    public int getSongsCount(){
    	return this.getAllSongs().size();
    }
    
    public boolean songExists(Song song){
    	boolean existance = false;
		for (Song songInPack : this.getAllSongs()){
			if (	songInPack.getPack().equals(song.getPack()) 
					&& songInPack.getTitle().equals(song.getTitle()) 
					&& songInPack.getDifficulty().equals(song.getDifficulty()) )
				existance = true;
		}
    	return existance;
    }
    
    public void updateSongResult(Song newSong) {
    	List<Song> newSonglist = new ArrayList<Song>();
    	if (this.songExists(newSong)) {
    		for (Song songInPack : this.getAllSongs()){
    			if (	songInPack.getPack().equals(newSong.getPack()) 
    					&& songInPack.getTitle().equals(newSong.getTitle()) 
    					&& songInPack.getDifficulty().equals(newSong.getDifficulty()) ) {
    				newSonglist.add(newSong);
    			} else {
    				newSonglist.add(songInPack);
    			}
    			
    		}
    	}
    songs = newSonglist;
    }
    
    public String generateXML(){
    	String result = null;
    	if (this.getSongsCount() > 0 && this.getName() != null){
    		result = "\t<pack name=\"" + this.getName() + "\">\n";
    		List <String> alreadyUsed = new ArrayList<String>();
    		for (Song currentSong : this.getAllSongs()){
    			if (!alreadyUsed.contains(currentSong.getTitle())){
    				result += "\t\t<song name=\"" + currentSong.getTitle() + "\">\n";
    				for (Song currentDifficulty : this.getAllDifficulties(currentSong.getTitle())){
    					result += "\t\t\t<difficulty name=\"" + currentDifficulty.getDifficulty() + "\" ";
    					result += "feet=\"" + Integer.toString(currentDifficulty.getFeet()) + "\" ";
    					if (currentDifficulty.getResult() > 0){
    						result += "result=\"" + Float.toString(currentDifficulty.getResult()) + "\" ";
    					}
    					result += "/>\n";
    				}
    				result += "\t\t</song>\n";
    				alreadyUsed.add(currentSong.getTitle());
    			}
    			
    		}
    		result += "\t</pack>\n";
    	}
    	return result;
    }
}
