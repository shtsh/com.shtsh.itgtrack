package com.shtsh.itgtrack;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	private SongsListAdapterWithoutPacks listViewAdapter;
	
	private boolean editEnabled;
	private TextView all;
	private SharedPreferences prefs;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //read preferences
        prefs = this.getSharedPreferences("com.sthsh.itgtrack", Context.MODE_PRIVATE);
        
        // Create working folder if not exist
        initiateSdFolders();
        
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.main);        

        MySQLiteHelper db = new MySQLiteHelper(this);
        
        editEnabled = true;
        // Add elements to layout
        LinearLayout ll = (LinearLayout) findViewById(R.id.mainLinearLayout);
        if (prefs.getBoolean("main_show_stats", false)) {     	
        	TextView avg = new TextView(this);
        	TextView cnt = new TextView(this);
        	all = new TextView(this);
        	Resources res = getResources();
        	avg.setText(res.getString(R.string.main_avg) + ": " + db.getAvgScore());
        	cnt.setText(res.getString(R.string.main_count_result) + ": " + db.getSongsCountWithResults());
        	all.setText(res.getString(R.string.main_count_all) + ": " + db.getSongsCount());
        	ll.addView(cnt);
        	ll.addView(avg);
        	ll.addView(all);
        }
        // Get All songs with results and show 10 closest to desired result
        int resultsCount = prefs.getInt("main_songs_count", 0);
        
        List<Song> songsWithResults = db.getAllSongsWithResults();
        db.close();
        Pack packToCreateListView = new Pack();
        
        if (songsWithResults.size() > 0) {
        	Collections.sort(songsWithResults);
        	//Collections.reverse(songsWithResults);
        	int i = 0;
        	for (Song song : songsWithResults) {
        		i++;
        		if(song.getResult() < 100)
        			packToCreateListView.addSong(song);
        		if (i >= resultsCount)
        			break;
        	}
        	listViewAdapter = new SongsListAdapterWithoutPacks(this, R.layout.list, packToCreateListView);
        	ListView listView = new ListView(this);
        	listView.setAdapter(listViewAdapter);
        	ll.addView(listView);
        	
        	final Intent editIntent = new Intent(this, EditResult.class);
        	listView.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					if (editEnabled) {
						Song song = (Song) listViewAdapter.getItem(arg2);
						editIntent.putExtra("song", song);
						startActivity(editIntent);
					}
				}
        		
        	});
        }
		
    }
    
    private void initiateSdFolders(){
        prefs.edit().putString("application_sdcard_directory", "/Android/data/com.shtsh.itgtrack" ).commit();
 		if (Environment.MEDIA_MOUNTED.equals(Environment
 				.getExternalStorageState())
 				&& !Environment.MEDIA_MOUNTED_READ_ONLY.equals(Environment
 						.getExternalStorageState())) {
 			
 			String appFolderName = Environment.getExternalStorageDirectory()
 					+ prefs.getString("application_sdcard_directory",
 							"Android/data/com.shtsh.itgtrack");
 			File appFolder = new File(appFolderName);
 			if (!appFolder.exists()) {
 				appFolder.mkdir();
 			}
 			
 			File noMedia = new File(appFolderName + "/.nomedia");
 			if (!noMedia.exists()) {
 				try {
 					noMedia.createNewFile();
 				} catch (IOException e) {
 					e.printStackTrace();
 				}
 			}
 		}
    }
    
    //Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        setProgressBarIndeterminateVisibility(false);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.main_add:
            	Intent intent_add = new Intent(this, AddResult.class);
            	startActivity(intent_add);
                return true;
            case R.id.main_list:
            	Intent intent_list = new Intent(this, ListResults.class);
            	startActivity(intent_list);
                return true;
            case R.id.main_options:
            	Intent intent_settings = new Intent(this, SettingsActivity.class);
            	startActivity(intent_settings);
            	finish();
            	return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}