package com.shtsh.itgtrack;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

public class SongListAdapter extends BaseExpandableListAdapter implements Filterable {
	private List<List<Song>> mGroups = new ArrayList<List<Song>>();	
	private List<List<Song>> fullGroups = new ArrayList<List<Song>>();
	private List<String> mPacks = new ArrayList<String>();
	private List<String> fullPacks = new ArrayList<String>();
	private static Context mContext;
	
	public int getPacksCount(){
		return mPacks.size();
	}
	
	public SongListAdapter (Context context, List<Pack> packs){

		mGroups.clear();
		for (int i=0; i<packs.size(); i++) 
			mGroups.add(packs.get(i).getAllSongs());
	    
		mContext = context;
		
		mPacks.clear();
		for (int i=0; i<packs.size(); i++)
			mPacks.add(packs.get(i).getName());
		
		fullPacks.clear();
		for (int i=0; i<packs.size(); i++)
			fullPacks.add(packs.get(i).getName());
		
		fullGroups.clear();
		for (int i=0; i<packs.size(); i++)
			fullGroups.add(packs.get(i).getAllSongs());
		
	}	
	

	@Override
	public Object getChild(int groupPosition, int childPosition) {
        return mGroups.get(groupPosition).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
	}

	@Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
            View convertView, ViewGroup parent) {
			
		if (convertView == null) {
					LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					convertView = inflater.inflate(R.layout.list, null);
		}
		
		Song song = mGroups.get(groupPosition).get(childPosition);
		if (song != null) {
			TextView topText = (TextView) convertView.findViewById(R.id.toptext);
			TextView bottomText = (TextView) convertView.findViewById(R.id.bottomtext);
			ImageView SongLetter = (ImageView) convertView.findViewById(R.id.songLetterImage);
			topText.setText(song.getTitle());                            
			bottomText.setText(song.getDifficulty() + " (" + song.getFeet() + ")");
        	
			int SongImageId = convertView.getResources().getIdentifier("com.shtsh.itgtrack:drawable/" + song.getLetterItgStyle(), null, null);
        	SongLetter.setImageResource(SongImageId);
        	//SVG svg = SVGParser.getSVGFromResource(convertView.getResources(), R.raw.a);
        	//SongLetter.setImageDrawable(svg.createPictureDrawable());
        	        	
        	TextView resultText = (TextView) convertView.findViewById(R.id.resultText);
        	resultText.setText(song.getResult() + "%");
        	
		}

		return convertView;
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		return mGroups.get(groupPosition).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mGroups.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return mPacks.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
        return groupPosition;
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.groupview, null);
		}
        
        TextView textGroup = (TextView) convertView.findViewById(R.id.pack);
        textGroup.setText(mPacks.get(groupPosition));
        
        ImageView plusImage = (ImageView) convertView.findViewById(R.id.plusImage);
        plusImage.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				
				String pack = mPacks.get(groupPosition);
				Intent editResultIntent = new Intent(mContext, AddResult.class);
				editResultIntent.putExtra("pack", pack);
				mContext.startActivity(editResultIntent);
			}
        	
        });
        	
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	@Override
	public Filter getFilter() {
		return new songsFilter();			
	}
	
    public void notifyDataSetInvalidated()
    {
        super.notifyDataSetInvalidated();
    }
	
    private class songsFilter extends Filter {
    	private List<String> newPackList = new ArrayList<String>();
    	
    	@Override
    	protected FilterResults performFiltering(CharSequence constraint) {
    		constraint = constraint.toString().toLowerCase();
            
    		FilterResults result = new FilterResults();
        	List<List<Song>> filtered = new ArrayList<List<Song>>();
        	
        	newPackList.clear();

            if (constraint != null ){

            	for (int songsPackNumber = 0 ; songsPackNumber < fullGroups.size(); songsPackNumber++){
            		List<Song> currentPack = fullGroups.get(songsPackNumber);
            		
            		List<Song> newPack = new ArrayList<Song>();
            		newPack.clear();

            		for (int songNumber = 0; songNumber <  currentPack.size(); songNumber++){
            			
            			Song currentSong = fullGroups.get(songsPackNumber).get(songNumber);            			
            			if(currentSong.getTitle().toLowerCase().contains(constraint)) {
            				newPack.add(currentSong);
            			} 
            			
            		}
            		
            		if (newPack.size() > 0) {
            			filtered.add(newPack);
            			newPackList.add(fullPacks.get(songsPackNumber));
            		} 
            	}
            	
            } 

            result.count = filtered.size();
            result.values = filtered;
            return result;            
    	}

		@SuppressWarnings("unchecked")
		@Override
    	protected void publishResults(CharSequence constraint, FilterResults result) {
    		if (result.count > 0){
    			mGroups = (List<List<Song>>) result.values;
    			mPacks.clear();
    			for(int i=0;i<newPackList.size();i++){
    				mPacks.add(newPackList.get(i));
    			} 
    			
    			notifyDataSetChanged();    			
    		} else {
    			notifyDataSetInvalidated();
    		}
    	}
    	
    }
}


