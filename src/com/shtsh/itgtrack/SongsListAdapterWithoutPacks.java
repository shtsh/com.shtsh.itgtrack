package com.shtsh.itgtrack;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SongsListAdapterWithoutPacks extends ArrayAdapter<Song> {
	
	private List<Song> packToMakeList;
	private Context context;
	
	public SongsListAdapterWithoutPacks(Context context, int textViewResourceId, Pack pack){
		super(context, textViewResourceId, pack.getAllSongs());
		packToMakeList = pack.getAllSongs();
		this.context = context;
	}
	
	public View getView(int position, View convertView, ViewGroup parent){
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.list, null);
		}
		
		Song song = packToMakeList.get(position);
		if (song != null) {
			TextView topText = (TextView) v.findViewById(R.id.toptext);
			TextView bottomText = (TextView) v.findViewById(R.id.bottomtext);
			ImageView SongLetter = (ImageView) v.findViewById(R.id.songLetterImage);
			topText.setText(song.getPack() + " / " + song.getTitle());                            
			bottomText.setText(song.getDifficulty() + " (" + song.getFeet() + ")");
        	
			int SongImageId = v.getResources().getIdentifier("com.shtsh.itgtrack:drawable/" + song.getLetterItgStyle(), null, null);
        	SongLetter.setImageResource(SongImageId);
        	        	
        	TextView resultText = (TextView) v.findViewById(R.id.resultText);
        	resultText.setText(song.getRemainingScoreToNext()+ "%");
		}
		
		return v;
		
	}

}
