package com.shtsh.itgtrack;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;


public class ListResults extends Activity { 
	private SongListAdapter mAdapter;	
	private ExpandableListView listView;
	private List<Pack> packs;
	boolean showAllSongs;
	MySQLiteHelper db;
	boolean noResults;
	private Menu listMenu;
	private SearchView searchView;
	private MenuItem searchViewItem;
	
	public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        showAllSongs = true;
        db = new MySQLiteHelper(this);	
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.fulllist);
		// Possibility to return to the main activity
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		listView = (ExpandableListView) findViewById(R.id.songs_list);
			
		List<String> packNames = new ArrayList<String>();
        packNames = db.getPackNamesWithResults();
        packs = new ArrayList<Pack>();
        
        if (packNames.size() > 0) {
        	showAllSongs = false;
        } else {
        	showAllSongs = true;
        	noResults = true;
        }
        
        final Intent i = new Intent(this, EditResult.class);
		
        listView.setOnChildClickListener(new OnChildClickListener() {
        	public boolean onChildClick(ExpandableListView parent, View v,
		            int groupPosition, int childPosition, long id) {
		            Song song =  (Song) mAdapter.getChild(groupPosition, childPosition);
		       		  // Starting activity to edit data
		       		i.putExtra("song", song);
		       		startActivity(i);
		       		return true;
					}
		
		});	
	   
    }
	
	//menu
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	this.listMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list, menu);
        if (noResults) {
        	MenuItem item = menu.findItem(R.id.list_all);
        	item.setVisible(false);
        } 
        searchViewItem = listMenu.findItem(R.id.menu_search);
        searchView = (SearchView) searchViewItem.getActionView();
        new ToggleSongs().execute("");
        searchViewItem.setVisible(true);
        
        return true;
    }
    
	public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent_home = new Intent(this, MainActivity.class);
        intent_home.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    switch (item.getItemId()) {
	        case android.R.id.home:
	            startActivity(intent_home);
	            return true;
	        case R.id.list_all:
	        	new ToggleSongs().execute("");
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
		
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		db.close();
	}

	private class ToggleSongs extends AsyncTask<String, Void, Void>{
   
		@Override
		protected Void doInBackground(String... params) {
		 	List<String> packNames = new ArrayList<String>();
	    	List<String> packNamesWithResults = new ArrayList<String>();
	    	packs.clear();
	    	db = new MySQLiteHelper(ListResults.this);	
	    	listView = (ExpandableListView) findViewById(R.id.songs_list);
	    	packNamesWithResults = db.getPackNamesWithResults();
	    	
	    	if (showAllSongs) {
	    		// show All songs	        		
	    		packNames = db.getPacks();
	            for (String packName : packNames) {
	            	Pack packFull = db.getPack(packName, false);
	            	List<Song> songsForPack= new ArrayList<Song>();
	            	songsForPack = packFull.getAllSongs();
	            	if (songsForPack.size() > 0) {
	            		Pack newPack = new Pack(packName, songsForPack);
	            		
	            		// Add results
	            		if (packNamesWithResults.contains(packName)) {
	            			Pack packWithResults = db.getPack(packName, true);
	            			for (Song song : packWithResults.getAllSongs()) {
	            				newPack.updateSongResult(song);
	            			}
	            		}
	            		
	            		packs.add(newPack);
	            	}
	            }
	    		showAllSongs = false;
	        } else {
	        	// show songs with result
	        	
	            for (String packName : packNamesWithResults) {
	            	Pack packWithResults = db.getPack(packName, true);
	            	if (packWithResults.getSongsCount() > 0) {
	            		packs.add(packWithResults);
	            	}
	            }
	        	showAllSongs = true;	
	        }    	
			return null;
		}
		
		protected void onPostExecute(Void results){
			if (packs.size() > 0) {
	        	mAdapter =  new SongListAdapter(ListResults.this,  packs);
	        	listView.setAdapter(mAdapter);
	        	setProgressBarIndeterminateVisibility(false);
	        	if (listMenu != null){
	        		MenuItem list = listMenu.findItem(R.id.list_all);
	        		list.setVisible(true);
	        		searchViewItem.setVisible(true);
	        		searchView.setOnQueryTextListener (new OnQueryTextListener () {

	        			@Override
	        			public boolean onQueryTextChange(String newText) {
	        					mAdapter.getFilter().filter(newText);
	        					if (mAdapter.getGroupCount()>0){
	        						for (int i=0; i<mAdapter.getGroupCount(); i++){
	        							listView.expandGroup(i);
	        						}
	        					}
	        					return true;
	        			}

	        			@Override
	        			public boolean onQueryTextSubmit(String newText) { 
	    						mAdapter.getFilter().filter(newText);
	    						if (mAdapter.getGroupCount()>0){
	    							for (int i=0; i<mAdapter.getGroupCount(); i++){
	    								listView.expandGroup(i);
	    							}
	    						}
	    	
	    						return true;
	        			}
	        		});
	        	}
	    	}
		}
		
		protected void onPreExecute(){
			setProgressBarIndeterminateVisibility(true);
			if (listMenu != null){
				MenuItem list = listMenu.findItem(R.id.list_all);
				list.setVisible(false);
				searchViewItem.setVisible(false);
			}
		}
	}
	
}