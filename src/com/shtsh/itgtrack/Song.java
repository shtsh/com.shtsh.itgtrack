package com.shtsh.itgtrack;

import java.math.BigDecimal;
import java.math.RoundingMode;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Song implements Parcelable, Comparable<Song> {
	
	private String title;
	private String pack;
	private String difficulty;
	private int feet;
	private float result;
	
	// Default constructor
	public Song(){
		title = "";
		pack = "";
		difficulty = "";
		feet = 0;
		result = 0;
	}

	public Song(String onePack){
		title = "";
		pack = onePack;
		difficulty = "";
		feet = 0;
		result = 0;
	}
	
	public void setTitle(String songTitle){
		this.title = songTitle;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public final void setPack(String songPack){
		this.pack = songPack;
	}
	
	public String getPack(){
		return this.pack;
	}
	
	public final void setDifficulty(String songDifficulty){
		this.difficulty = songDifficulty;
	}
	
	public String getDifficulty(){
		return this.difficulty;
	}
	
	public final void setFeet(int songFeet){
		this.feet = songFeet;
	}	
	
	public int getFeet(){
		return this.feet;
	}
	
	public final void setResult(float songResult){
		this.result = songResult;
	}	
	
	public float getResult(){
		return this.result;
	}
	
	// Returns image file name depending on score
	public String getLetterItgStyle(){
		float result = this.getResult();
		if (result > 100) {
			return "incorrect";
		} else if (result == 100){
			return "sssstar";
		} else if (result >= 99) {
			return "ssstar";
		} else if (result >= 98) {
			return "sstar";
		} else if (result >= 96) {
			return "star";
		} else if (result >= 94) {
			return "splus";
		} else if (result >= 92) {
			return "s";
		} else if (result >=89) {
			return "sminus";
		} else if (result >= 86) {
			return "aplus";
		} else if (result >= 83) {
			return "a";
		} else if (result >= 80) {
			return "aminus";
		} else if (result >= 76) {
			return "bplus";
		} else if (result >= 72) {
			return "b";
		} else if (result >= 68) {
			return "bminus";
		} else if (result >= 64) {
			return "cplus";
		} else if (result >= 60) {
			return "c";
		} else if (result >= 55) {
			return "cminus";
		} else if (result < 55 && result > 0) {
			return "d";
		}
		return "f";
	}

	// Methods to put Song object to other activity are below
	@Override
	public int describeContents() {
		return 0;
	}

	// elements to send via Parcel. Order is important!
	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeString(title);
		arg0.writeString(pack);
		arg0.writeString(difficulty);
		arg0.writeInt(feet);
		arg0.writeFloat(result);
	}
	
	 // this is used to regenerate your object. All Parcelables must have a CREATOR that implements these two methods
    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    // Constructor to read elements from parcel. Order is important!
    private Song (Parcel in) {
    	title = in.readString();
    	pack = in.readString();
    	difficulty = in.readString();
    	feet = in.readInt();
    	result = in.readFloat();
    }
    
    public void log(){	
    	Log.d("Song data", "-------------------------------");
    	Log.d("Pack: ", this.getPack());
    	Log.d("Song:", this.getTitle());
    	Log.d("Diffculty", this.getDifficulty());
    	Log.d("Feet:", Integer.toString(this.getFeet()));
    	Log.d("Result:", Float.toString(this.getResult()));
    }
    
	// Returns image file name depending on score
	public float getDesiredResult(){
		float result = this.getResult();
		if ( result < 60) {
			return 60;
		} else if  (result < 70) {
			return 70;
		} else if (result < 80) {
			return 80;
		} else if (result < 90) {
			return 90;
		} else if (result < 96) {
			return 96;
		} else if (result < 98) {
			return 98;
		} else if (result < 99) {
			return 99;
		} else if (result < 99.5) {
			return (float) 99.5;
		} else {
			return 100;
		}
	}
	
	private float getPreviousResult(){
		float result = this.getResult();		
		if (result > 99.5) {
			return (float) 99.5;
		} else if (result > 99) {
			return 99;
		} else if (result > 98) {
			return 98;
		} else if (result > 96) {
			return 96;
		} else if (result > 90) {
			return 90;
		} else if (result > 80) {
			return 80;
		} else if (result > 70) {
			return 70;
		} else if (result > 60) {
			return 60;
		} else 
			return 0;
		
	}
	
	public float getRemainingScoreToNext(){
		float currentScore = this.getResult();
		float nextScore = this.getDesiredResult();	
		float previousScore = this.getPreviousResult();
		float percentage = (nextScore - currentScore) / (nextScore - previousScore) * 100;		
		BigDecimal bd = new BigDecimal(percentage);
		BigDecimal res = bd.setScale(2, RoundingMode.HALF_UP);
		percentage = res.floatValue();
		return percentage;

	}

	@Override
	public int compareTo(Song newSong) {
		return Math.round(this.getRemainingScoreToNext() - newSong.getRemainingScoreToNext());
	}
	
	public String generateFileName(){
		String fileName = "/" + this.getPack().toLowerCase().replace("\'", "").replace(" ", "") + "_" 
				+ this.getTitle().toLowerCase().replace("\'", "").replace(" ", "") + "_" 
				+ this.getDifficulty().toLowerCase().replace("\'", "").replace(" ", "");
		return  fileName;
	}
    
}
