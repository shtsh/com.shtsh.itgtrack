package com.shtsh.itgtrack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.os.Vibrator;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.util.Log;
import android.widget.Toast;

public class SettingsFragment extends PreferenceFragment {
	private SharedPreferences prefs;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        
        prefs = getActivity().getSharedPreferences("com.sthsh.itgtrack", Context.MODE_PRIVATE);
        final String appFolder = prefs.getString("application_sdcard_directory", "/Android/data/com.shtsh.itgtrack");
        Preference restore = (Preference) findPreference("data_restore");
        Preference backup = (Preference) findPreference("data_backup");
        Preference load = (Preference) findPreference("data_load");

        //check if folder is available
        String state = Environment.getExternalStorageState();
        
		if (!Environment.MEDIA_MOUNTED.equals(state)) {
			restore.setEnabled(false);
			backup.setEnabled(false);
			load.setEnabled(false);
		} else {
			if (new File(Environment.getExternalStorageDirectory() + appFolder)
					.isDirectory()) {
				if (!Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
					backup.setOnPreferenceClickListener(new OnPreferenceClickListener() {
						@Override
						public boolean onPreferenceClick(Preference preference) {
							new BackupData().execute(appFolder + "/backup.xml");
							return true;
						}
					});
				} else {
					backup.setEnabled(false);
				}

				restore.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					@Override
					public boolean onPreferenceClick(Preference preference) {
						new ParseXmlTask().execute(appFolder + "/backup.xml");
						return true;
					}
				});
			} else {
				restore.setEnabled(false);
				backup.setEnabled(false);
			}

			load.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				@Override
				public boolean onPreferenceClick(Preference preference) {
					new ParseXmlTask().execute("/itgtrack.xml");
					return true;
				}
			});
			
		}
        
		final
        EditTextPreference songsCount = (EditTextPreference) findPreference("main_songs_count");
        songsCount.setText(Integer.toString(prefs.getInt("main_songs_count", 0)));
        songsCount.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				prefs.edit().putInt("main_songs_count", Integer.valueOf((String) newValue)).commit();
				return true;
			}
		});
        
        CheckBoxPreference showStats = (CheckBoxPreference) findPreference("main_show_stats");
        showStats.setChecked(prefs.getBoolean("main_show_stats", false));     
        showStats.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				prefs.edit().putBoolean("main_show_stats", (Boolean) newValue).commit();
				return true;
			}
		});
        
        Preference share = (Preference) findPreference("about_share");
        share.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				String message = "I'm using ITGTrack";
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("text/plain");
				share.putExtra(Intent.EXTRA_TEXT, message);
				startActivity(Intent.createChooser(share, "Share result"));
				return true;
			}
		});
        
        Preference about = (Preference) findPreference("about_about");
        PackageInfo pInfo = null;
		try {
			pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final Resources res = getActivity().getResources();
        about.setTitle(res.getString(R.string.app_name) + " " + pInfo.versionName);
        
        Preference contact = (Preference) findPreference("about_contact");
        contact.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				PackageInfo pInfoForEmail = null;
				try {
					pInfoForEmail = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"liparinai@gmail.com"});
				i.putExtra(Intent.EXTRA_SUBJECT, res.getString(R.string.app_name) + " " + pInfoForEmail.versionName);
				//i.putExtra(Intent.EXTRA_TEXT   , "body of email");
				try {
				    startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
				    Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
				}
				return true;
			}
		});
    }
    
private class ParseXmlTask extends AsyncTask<String, Void, Void> {
		
		private int errorType;
		private PowerManager.WakeLock wl;
		private long startTime;
		private PowerManager pm;
		
		@Override
		protected Void doInBackground(String... params) {
			Log.d("PARAMS",Environment.getExternalStorageDirectory() + params[0]);
			long startTime = System.currentTimeMillis();
            PacksXmlParser packsParser = new PacksXmlParser();
            File file = new File(Environment.getExternalStorageDirectory() + params[0]);

            FileInputStream fis;
    		try {
    			
    			fis = new FileInputStream(file);
    			packsParser.parse(getActivity(), fis);
    			errorType = 0;
    		} catch (FileNotFoundException e) {
    			errorType = 1;   			
    			Log.e("FileNotFoundException", "Cannot open File");
    		} catch (XmlPullParserException e) {
    			errorType = 2; 
    			Log.e("XmlPullParserException", "Cannot Parse");
    		} catch (IOException e) {
    			Log.e("IOException", "IOException");
    		} finally {
    			Log.d("XML Parsing", Long.toString(System.currentTimeMillis() - startTime) + " ms");
    		}
			return null;
		}

		@SuppressWarnings("static-access")
		protected void onPreExecute(){
			Toast.makeText(getActivity(), "Import started. Please wait", Toast.LENGTH_SHORT).show();
			getActivity().setProgressBarIndeterminateVisibility(true);
			pm = (PowerManager) getActivity().getSystemService(getActivity().POWER_SERVICE);
			wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
			wl.acquire();
			
			startTime = System.currentTimeMillis();

		}
		
		protected void onPostExecute(Void results){

			getActivity().setProgressBarIndeterminateVisibility(false);
			switch (errorType){
			case 0:
				Toast.makeText(getActivity(), "Import completed", Toast.LENGTH_SHORT).show();
				break;
			case 1: 
				Toast.makeText(getActivity(), "Cannot open file", Toast.LENGTH_SHORT).show();
				break;
			case 2:
				Toast.makeText(getActivity(), "Pasing Error", Toast.LENGTH_SHORT).show();
				break;
			default:
				break;	
			}
			Vibrator v = (Vibrator) getActivity().getSystemService(MainActivity.VIBRATOR_SERVICE);
			v.vibrate(300);
			wl.release();
			
			long endTime = System.currentTimeMillis();
			Log.d("Time to parse XML", Long.toString(endTime - startTime) + " ms");
			 
		}

	}

private class BackupData extends AsyncTask<String, Void, Void> {
	
	private int errorType;
	private PowerManager.WakeLock wl;
	private long startTime;
	private PowerManager pm;
	
	@Override
	protected Void doInBackground(String... params) {
		long startTime = System.currentTimeMillis();
        File file = new File(Environment.getExternalStorageDirectory() + params[0]);

        FileOutputStream fos;
		try {	
			if (file.exists ()) file.delete ();
			fos = new FileOutputStream(file);
						
			MySQLiteHelper db = new MySQLiteHelper(getActivity());
			String dataToWrite = "<database>\n";
			for (String packName : db.getPackNamesWithResults()){
				dataToWrite += db.getPack(packName, true).generateXML();
			}
			dataToWrite += "</database>\n";
	        db.close();
	        
	        fos.write(dataToWrite.getBytes());
	        fos.flush();
	        fos.close();
	               
			errorType = 0;
		} catch (FileNotFoundException e) {
			errorType = 1;   			
			Log.e("FileNotFoundException", "Cannot open File");
		} catch (IOException e) {
			Log.e("IOException", "IOException");
		} finally {
			Log.d("Data Backup Time", Long.toString(System.currentTimeMillis() - startTime) + " ms");
		}
		return null;
	}

	@SuppressWarnings("static-access")
	protected void onPreExecute(){
		Toast.makeText(getActivity(), "Export started. Please wait", Toast.LENGTH_SHORT).show();
		getActivity().setProgressBarIndeterminateVisibility(true);
		pm = (PowerManager) getActivity().getSystemService(getActivity().POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
		wl.acquire();
		
		startTime = System.currentTimeMillis();

	}
	
	protected void onPostExecute(Void results){

		getActivity().setProgressBarIndeterminateVisibility(false);
		switch (errorType){
		case 0:
			Toast.makeText(getActivity(), "Export completed", Toast.LENGTH_SHORT).show();
			break;
		case 1: 
			Toast.makeText(getActivity(), "Cannot open file", Toast.LENGTH_SHORT).show();
			break;
		default:
			break;	
		}
		Vibrator v = (Vibrator) getActivity().getSystemService(MainActivity.VIBRATOR_SERVICE);
		v.vibrate(300);
		wl.release();
		
		long endTime = System.currentTimeMillis();
		Log.d("Time to write XML", Long.toString(endTime - startTime) + " ms");
		 
	}

}

}