package com.shtsh.itgtrack;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

public class SettingsActivity extends Activity {
	
	private Intent intent_home;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		intent_home = new Intent(this, MainActivity.class);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		
		// Possibility to return to the main activity
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		getFragmentManager().beginTransaction()
			.replace(android.R.id.content, new SettingsFragment())
			.commit();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        setProgressBarIndeterminateVisibility(false);
        return true;
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			startActivity(intent_home);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        startActivity(intent_home);
	        finish();
	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
		
	}
}