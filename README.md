ITG Tracker
=========

Application for android phones to records and manage results.

This application requires Android 4.0.3+

Application supports import packs and songs from XML file.
The file should be located in the root to SD Card and should be named as itgtrack.xml.
Example of this file you could see in files folder.

You can download built application using this link:
https://github.com/Shtsh/com.shtsh.itgtrack/blob/master/Files/com.shtsh.itgtrack-0.1.apk?raw=true

